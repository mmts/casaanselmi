'use strict';

/**
 * @ngdoc overview
 * @name casaAnselmiApp
 * @description
 * # casaAnselmiApp
 *
 * Main module of the application.
 */
angular
  .module('casaAnselmiApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'toastr',
    'pascalprecht.translate',
    'ngLodash',
    'angular-carousel',
    'ui.bootstrap'
  ])

  .config(['$translateProvider', function ($translateProvider) {
    $translateProvider.useStaticFilesLoader({
      prefix: '/translate/',
      suffix: '.json'
    });
    $translateProvider.preferredLanguage('es');
    $translateProvider.useSanitizeValueStrategy('escape');
  }])
  .config(['$routeProvider',function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home.html',
        controller: 'HomeCtrl',
        controllerAs: 'home'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl',
        controllerAs: 'about'
      })
      .when('/contact', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl',
        controllerAs: 'contact'
      })
      .when('/projects', {
        templateUrl: 'views/projects.html',
        controller: 'ProjectsCtrl',
        controllerAs: 'projects'
      })
      .when('/house', {
        templateUrl: 'views/house.html',
        controller: 'HouseCtrl',
        controllerAs: 'house'
      })
      .when('/project/:projectId', {
        templateUrl: 'views/projectDetail.html',
        controller: 'ProjectDetailCtrl',
        controllerAs: 'project'
      })
      .otherwise({
        redirectTo: '/'
      });
  }]);
