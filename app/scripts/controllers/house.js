'use strict';

/**
 * @ngdoc function
 * @name casaAnselmiApp.controller:AboutCtrl
 * @description
 * # HouseCtrl
 * Controller of the casaAnselmiApp
 */
angular.module('casaAnselmiApp')
  .controller('HouseCtrl',['$http','$scope','toastr', function ($http,$scope,toastr) {
    $http.get('data/house.json')
      .success(function(data){
        $scope.houseanselmi = data;
        $scope.setInterval=5000;
      })
      .error(function(){
        toastr.error('Ocurrió un error cargando la información.');
      });
  }]);
