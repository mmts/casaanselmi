'use strict';

/**
 * @ngdoc function
 * @name casaAnselmiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the casaAnselmiApp
 */
angular.module('casaAnselmiApp')
  .controller('ProjectDetailCtrl',['$http','$scope','toastr','lodash','$routeParams','$window', function ($http,$scope,toastr,_,$routeParams,$window) {
    $http.get('data/projects.json')
      .success(function(data){
        $scope.project = _.first(_.filter(data, function(p){ return p.id === parseInt($routeParams.projectId); })) ;
      })
      .error(function(){
        toastr.error('Ocurrió un error cargando los proyectos.');
      });

    $(window).resize(function () {
      $scope.widthP = $window.innerWidth;
    });
  }]);
