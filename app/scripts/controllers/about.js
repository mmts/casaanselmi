'use strict';

/**
 * @ngdoc function
 * @name casaAnselmiApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the casaAnselmiApp
 */
angular.module('casaAnselmiApp')
  .controller('AboutCtrl',['$http','$scope','toastr', function ($http,$scope,toastr) {

    $http.get('data/about.json')
      .success(function(data){
        $scope.biography = data;
      })
      .error(function(){
        toastr.error('Ocurrió un error cargando la información.');
      });

    $scope.getBiografy = function (desc) {
      if ($scope.lang === 'es') {
        return desc.descriptionEs;
      } else {
        return desc.descriptionEn;
      }
    }

  }]
);
