'use strict';

/**
 * @ngdoc function
 * @name casaAnselmiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the casaAnselmiApp
 */
angular.module('casaAnselmiApp')
  .controller('ProjectsCtrl',['$http','$scope','toastr','lodash','$translate' ,function ($http,$scope,toastr,_,$translate) {
    $scope.logo = "images/logo.png";

    $http.get('data/projects.json')
      .success(function(data){
        $scope.projects = data;
        $scope.loadTypology(data);

      })
      .error(function(){
        toastr.error('Ocurrió un error cargando los proyectos.');
      });

    $http.get('data/filters.json')
      .success(function(data){
        $scope.filters = data;

      })
      .error(function(){
        toastr.error('Ocurrió un error cargando los proyectos.');
      });


    $scope.loadTypology = function(projects){
      $scope.typologies = (_.uniq(projects ,'typologyEs')).concat([{'typologyEs' :'Todos', 'typologyEn' :'All'}]);
    };


    $scope.setFilter = function(t){
      if ($scope.lang === 'es') {
        if(t.typologyEs === 'Todos') {
          $scope.filterT = undefined;
        } else {
          $scope.filterT = t;
        }
      } else {
        if(t.typologyEn === 'All') {
          $scope.filterT = undefined;
        } else {
          $scope.filterT = t;
        }
      }
    };

    $scope.setFilterData = function(d) {
      if(d.startYear === 'Todos' || d.startYear === 'All'){
        $scope.filterRange = undefined;
      }else{
        $scope.filterRange = d;
      }
    };

    $scope.checkRange = function(p) {
      if ($scope.filterRange !== undefined){
        if (parseInt($scope.filterRange.startYear) <= parseInt(p.startYear) && parseInt(p.startYear) <= parseInt($scope.filterRange.endYear) ||
            parseInt($scope.filterRange.startYear) <= parseInt(p.endYear) && parseInt(p.endYear) <= parseInt($scope.filterRange.endYear)) {
          return true;
        } else {
          return false;
        }
      } else {
        return true;
      }
    };

    $scope.getTypology = function (t) {
      if ($scope.lang === 'es') {
        return t.typologyEs;
      } else {
        return t.typologyEn;
      }
    };

    $scope.getOrder = function () {
      if ($scope.lang === 'es') {
        return '+typologyEs';
      } else {
        return '+typologyEn';
      }
    }

    $scope.getRange = function (range) {
      if ($scope.lang === 'es') {
        return range.startYearEs;
      } else {
        return range.startYearEn;
      }
    }

    $scope.getOrderRange = function () {
      if ($scope.lang === 'es') {
        return '+startYearEs';
      } else {
        return '+startYearEn';
      }
    }

  }]);
