'use strict';

/**
 * @ngdoc function
 * @name casaAnselmiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the casaAnselmiApp
 */
angular.module('casaAnselmiApp')
  .controller('HomeCtrl',['$http', '$scope', '$window', 'toastr', '$translate', function ($http, $scope, $window, toastr, $translate) {
    $scope.logo = "images/logo.png";
    $scope.widthP = $window.innerWidth;
    $scope.lang = "es";

    $http.get('data/projects.json')
      .success(function(data){
        $scope.project = data[Math.floor((Math.random() * data.length) + 1)];
      })
      .error(function(){
        toastr.error('Ocurrió un error cargando los proyectos.');
      });

    $(window).resize(function () {
      $scope.widthP = $window.innerWidth;
    });

    $scope.changeLanguage = function (langKey) {
      $translate.use(langKey);
      $scope.lang = langKey;
    };

  }]);
