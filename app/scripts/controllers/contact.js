'use strict';

/**
 * @ngdoc function
 * @name casaAnselmiApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the casaAnselmiApp
 */
angular.module('casaAnselmiApp')
  .controller('ContactCtrl',['$http','$scope','toastr','$window',function ($http,$scope,toastr,$window) {

    $http.get('data/contact.json')
      .success(function(data){
        $scope.contact = data;
      })
      .error(function(){
        toastr.error('Ocurrió un error cargando los datos de contacto.');
      });

    $scope.sendEmail = function(email){
      alert(JSON.stringify(email));
      //TODO call service to send email
    };

    function showMap() {
      var map, marker = '';
      if ($window.innerWidth < 768) {
        map = new google.maps.Map(document.getElementById('container-map-ms'), {
          zoom: 18,
          center: new google.maps.LatLng(10.48968,	-66.86414),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: true,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          }
        });

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(10.491168, -66.864058),
          map:map,
          animation: google.maps.Animation.BOUNCE,
          title:"MachineSoft Desarrollando tu potencial"
        });
      } else {
        map = new google.maps.Map(document.getElementById('container-map-ms'), {
          zoom: 18,
          center: new google.maps.LatLng(10.48968,	-66.86414),
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: true,
          mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
          }
        });

        marker = new google.maps.Marker({
          position: new google.maps.LatLng(10.489868, -66.865958),
          map:map,
          animation: google.maps.Animation.BOUNCE,
          title:"MachineSoft Desarrollando tu potencial"
        });
      }
    }

    showMap();
    $(window).resize(function () {
      showMap();
    });


  }]);
