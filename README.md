# Casa Anselmi

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Clone Repository

     git clone https://mmts@bitbucket.org/mmts/casaanselmi.git
     
## Install Dependencies

     cd angular-demo
     npm install

## Run Grunt

     nvm use 0.12
     grunt serve

## Access Angular App

Open browser http://localhost:9000/

## Testing

Run the unit tests with karma:

     grunt test
